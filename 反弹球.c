#include <conio.h>
#include <graphics.h>

#define High 480  // 游戏画面尺寸
#define Width 640
#define brick_num 16

// 全局变量
int ball_x, ball_y; // 小球的坐标
int ball_vx, ball_vy; // 小球的速度
int radius;  // 小球的半径
int rectangle_x, rectangle_y;//中心坐标
int rectangle_high, rectangle_width;//高宽
int rectangle_left, rectangle_right, rectangle_top, rectangle_bottom;//挡板上下左右边界
int rectangle_vx, rectangle_vy;//移动速度
int brickexist[brick_num];

void startup()  // 数据初始化
{
	int i;

	ball_x = Width / 2;
	ball_y = High / 2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 20;

	rectangle_vx = 40;
	rectangle_vy = 10;
	rectangle_x = 320;
	rectangle_y = 465;
	rectangle_high = 30;
	rectangle_width = 320;
	rectangle_left = rectangle_x - rectangle_width / 2;
	rectangle_right = rectangle_x + rectangle_width / 2;
	rectangle_top = rectangle_y - rectangle_high / 2;
	rectangle_bottom = rectangle_y + rectangle_high / 2;

	for (i = 0; i < brick_num; i++)
		brickexist[i] = 1;

	initgraph(Width, High);
	BeginBatchDraw();
}

void clean()  // 显示画面
{
	int i;
	// 绘制黑线、黑色填充的圆
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball_x, ball_y, radius);
	bar(rectangle_left, rectangle_top, rectangle_right, rectangle_bottom);
	for (i = 0; i < brick_num; i++)
		if (brickexist[i] == 0)
		{
			fillrectangle(i*Width / brick_num, 0, (i + 1)*Width / brick_num, Width / brick_num);
		}
}

void show()  // 显示画面
{
	int i;
	// 绘制黄线、绿色填充的圆
	setcolor(YELLOW);
	setfillcolor(GREEN);
	fillcircle(ball_x, ball_y, radius);

	setfillcolor(GREEN);
	bar(rectangle_left, rectangle_top, rectangle_right, rectangle_bottom);

	setcolor(WHITE);
	setfillcolor(RED);
	for (i = 0; i < brick_num; i++)
	{
		if (brickexist[i] != 0)
			fillrectangle(i*Width / brick_num, 0, (i + 1)*Width / brick_num, Width / brick_num);
	}
	FlushBatchDraw();
	// 延时
	Sleep(3);
}

void updateWithoutInput()  // 与用户输入无关的更新
{
	int i;
	// 更新小圆坐标
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;

	if (ball_y + radius == rectangle_top && ball_x >= rectangle_left && ball_x <= rectangle_right)
		ball_vy = -ball_vy;
	if ((ball_x <= radius) || (ball_x >= Width - radius))
		ball_vx = -ball_vx;
	if ((ball_y <= radius) || (ball_y >= High - radius))
		ball_vy = -ball_vy;

	for (i = 0; i < brick_num; i++)
	{
		if (brickexist[i] != 0)
		{
			if (ball_y - radius == Width / brick_num && ball_x >= i * Width / brick_num && ball_x <= (i + 1)* Width / brick_num)
			{
				ball_vy = -ball_vy;
				brickexist[i] = 0;
			}
		}
	}
}

void updateWithInput()  // 与用户输入有关的更新
{
	char input;
	if (kbhit())
	{
		input = getch();
		if (input == 'a')
		{
			if (rectangle_left > 0)
			{
				rectangle_x -= rectangle_vx;
				rectangle_left = rectangle_x - rectangle_width / 2;
				rectangle_right = rectangle_x + rectangle_width / 2;
				rectangle_top = rectangle_y - rectangle_high / 2;
				rectangle_bottom = rectangle_y + rectangle_high / 2;
			}
		}
		if (input == 'd')
		{
			if (rectangle_right < Width)
			{
				rectangle_x += rectangle_vx;
				rectangle_left = rectangle_x - rectangle_width / 2;
				rectangle_right = rectangle_x + rectangle_width / 2;
				rectangle_top = rectangle_y - rectangle_high / 2;
				rectangle_bottom = rectangle_y + rectangle_high / 2;
			}
		}
		if (input == 'w')
		{
			if (rectangle_top > 0)
			{
				rectangle_y -= rectangle_vy;
				rectangle_left = rectangle_x - rectangle_width / 2;
				rectangle_right = rectangle_x + rectangle_width / 2;
				rectangle_top = rectangle_y - rectangle_high / 2;
				rectangle_bottom = rectangle_y + rectangle_high / 2;
			}
		}
		if (input == 's')
		{
			if (rectangle_bottom < High)
				rectangle_y += rectangle_vy;
				rectangle_left = rectangle_x - rectangle_width / 2;
				rectangle_right = rectangle_x + rectangle_width / 2;
				rectangle_top = rectangle_y - rectangle_high / 2;
				rectangle_bottom = rectangle_y + rectangle_high / 2;
		}
	}
}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		clean();  // 把之前绘制的内容清除
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
		show();  // 显示新画面
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}